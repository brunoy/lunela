#!/bin/bash

#
# Uninstall lunela
# Bruno Rome <bruno (at) mailoo (dot) org>
# https://gitlab.com/brunoy/lunela.git
#

name='lunela'

rm -rf /usr/share/$name
rm -f /usr/bin/$name

exit 0
