#!/usr/bin/env python
# -*- coding:Utf-8 -*-

#
#    Author : Bruno Rome <bruno (at) mailoo (dot) org>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


# Modules python
from curses import wrapper
from sys import version_info

# Lunela module
from moon.draw_window import drawCursesWindow, usage
from moon.names import code_error


def main():

    # Vérif version
    if version_info < (3, 6):
        # For astimezone() de datetime.
        print("python 3.6 et supérieur requis")
        return 0

    # C'est parti ...
    value = wrapper(drawCursesWindow)

    # Code error
    if value == -1: print("{} : 76x10".format(code_error[0]))
    elif value == -3: print("{}".format(code_error[1]))
    elif value == -4: print("{}".format(code_error[2]))
    elif value == -5: print("{} 'lunela --help'".format(code_error[3]))
    elif value == -6: print("{} 'lunela --help'".format(code_error[4]))
    elif value == -7: print("{}".format(code_error[5]))
    elif value == -9: usage()

    # Exit
    return 0

if __name__ == "__main__":
    main()
