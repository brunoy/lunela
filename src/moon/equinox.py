##
#            Equinoxes and Solstices.
#
#                  Chapter 27
##


# Python module
from math import cos, radians, floor

# Moon modules
from .functions import deltaT, jd2date
from .tables import sumS27C
from .names import nameES

class EquinoxSolstice:
    """ Calculation equinoxes and solstices. """
    def __init__(self, yd, dtz):

        # Delta time zone utc or hour pc
        self.deltaTZ = dtz

        self.year = floor(yd)
        self.DT = deltaT(yd) / 86400

    def equinoxSol(self):
        if self.year > 1000:
            return self.calcul(self.yearMore1000(self.year))
        else:
            return self.calcul(self.yearLess1000(self.year))

    def calcul(self, JDEos):
        equSol = []
        i = 0
        for JDEo in JDEos:
            jde = self.JDE(JDEo) - self.DT  # TD to UT
            jde += self.deltaTZ             # in Hour computer or UT
            equSol.append( jd2date(jde) + (nameES[i],) )
            i += 1
        return equSol

    def JDE(self, JDEo):
        T = (JDEo - 2451545.0) / 36525
        W = (35999.373 * T) - 2.47
        Dl = 1 + (0.0334 * cos(radians(W))) + (0.0007 * cos(radians(W + W)))
        return JDEo + ( (0.00001 * sumS27C(T)) / Dl )

    # for the years -1000 to +1000
    def yearLess1000(self, year): # Table 27.A

        Y  = year / 1000
        Y2 = pow(Y, 2)
        Y3 = pow(Y, 3)
        Y4 = pow(Y, 4)

        # March equinoxe (beginning of astronomical spring)
        spring =  1721139.29189 + (365242.13740 * Y)\
                                + (0.06134 * Y2)\
                                + (0.00111 * Y3)\
                                - (0.00071 * Y4)

        # June solstice
        summer =  1721233.25401 + (365241.72562 * Y)\
                                - (0.05323 * Y2)\
                                + (0.00907 * Y3)\
                                + (0.00025 * Y4)

        # September equinox
        autumn =  1721325.70455 + (365242.49558 * Y)\
                                - (0.11677 * Y2)\
                                - (0.00297 * Y3)\
                                + (0.00074 * Y4)

        # December solstice
        winter =  1721414.39987 + (365242.88257 * Y)\
                                - (0.00769 * Y2)\
                                - (0.00933 * Y3)\
                                - (0.00006 * Y4)

        return (spring, summer, autumn, winter)

    # for the years +1000 to +3000
    def yearMore1000(self, year): # Table 27.B

        Y  = (year - 2000) / 1000
        Y2 = pow(Y, 2)
        Y3 = pow(Y, 3)
        Y4 = pow(Y, 4)

        # March equinoxe
        spring = 2451623.80984 + (365242.37404 * Y)\
                               + (0.05169 * Y2)\
                               - (0.00411 * Y3)\
                               - (0.000057 * Y4)

        # June solstice
        summer = 2451716.56767 + (365241.62603 * Y)\
                               + (0.00325 * Y2)\
                               + (0.00888 * Y3)\
                               - (0.00030 * Y4)

        # September equinox
        autumn = 2451810.21715 + (365242.01767 * Y)\
                               - (0.11575 * Y2)\
                               + (0.00337 * Y3)\
                               + (0.00078 * Y4)

        # December solstice
        winter = 2451900.05952 + (365242.74049 * Y)\
                               - (0.06233 * Y2)\
                               - (0.00823 * Y3)\
                               + (0.00032 * Y4)

        return (spring, summer, autumn, winter)
