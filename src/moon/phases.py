##
#           Phases of the moon.
#
#               Chapter 49
##


# Python module
from math import ceil, floor

# Lunela modules
from .names import phase, namePhase
from .functions import jd2date, deltaT, hms2jdec, decJ2jhm
from .tables import (correctionNewMoon49, correctionFullMoon49, correctionW49,
                     correctionFirstLastQuarter49)


class Phases:
    """ Calculation phases of the moon. """
    def __init__(self, jd, hd, yd, tz):

        # Time zone
        self.tz = tz

        # const synodic period of the moon
        self.SYNOD = 29.530589

        # year decimal
        year = yd

        # DeltaT
        self.DT = deltaT(year) / 86400

        # Calcul k (49.2)
        self.k = (year - 2000) * 12.3685

        # k current phase
        self.k = ceil(self.k)
        self.k = self.currentK(jd, self.k)
        self.currentPhase = self.k - floor(self.k)

        # Values phase
        self.new, self.first, self.full, self.last = 0.0, 0.25, 0.50, 0.75

        # Current values phase
        self.phases = self.upPhases(self.currentPhase)

        # jde new moon current cycle
        self.jde = self.calculPh(floor(self.k), self.new)
        self.age = (jd + hd + tz) - self.jde
        # Ajustement , À revoir ###################
        if self.age > self.SYNOD: self.age -= self.SYNOD
        if self.age < 0: self.age += self.SYNOD

        self.next = 1

    def ageMoon(self):
        return decJ2jhm(self.age)

    # Name current phase
    def name(self):

        numPhase = floor(self.age * (8 / self.SYNOD))
        return namePhase[numPhase]

    # Num current phase
    def num(self):
        return floor(self.age * (8 / self.SYNOD))

    def newMoon(self):
        updatek = self.k + self.phases["new"]
        jde = self.calculPh(updatek, self.new)
        return jd2date(jde) + (phase[self.new],)

    def firstQuarter(self):
        updatek = self.k + self.phases["first"]
        jde = self.calculPh(updatek, self.first)
        return jd2date(jde) + (phase[self.first],)

    def fullMoon(self):
        updatek = self.k + self.phases["full"]
        jde = self.calculPh(updatek, self.full)
        return jd2date(jde) + (phase[self.full],)

    def lastQuarter(self):
        updatek = self.k + self.phases["last"]
        jde = self.calculPh(updatek, self.last)
        return jd2date(jde) + (phase[self.last],)

    def nextNewMoon(self):
        updatek = self.k + self.phases["new"] + self.next
        jde = self.calculPh(updatek, phase["new"])
        return jd2date(jde) + (phase[phase["new"]],)

    def nextFirstQuarter(self):
        updatek = self.k + self.phases["first"] + self.next
        jde = self.calculPh(updatek, self.first)
        return jd2date(jde) + (phase[self.first],)

    def nextFullMoon(self):
        updatek = self.k + self.phases["full"] + self.next
        jde = self.calculPh(updatek, self.full)
        return jd2date(jde) + (phase[self.full],)

    def nextLastQuarter(self):
        updatek = self.k + self.phases["last"] + self.next
        jde = self.calculPh(updatek, self.last)
        return jd2date(jde) + (phase[self.last],)

    def cycle(self):
        k = self.k
        return self.calculCycle(k)

    def nextCycle(self):
        k = self.k + self.next
        return self.calculCycle(k)

    def calculCycle(self, k):
        cy = []
        p = self.currentPhase
        step = 0.25
        for i in range(4):

            jde = self.calculPh(k, p)
            cy.append( jd2date(jde) + (phase[p],) )

            k += step

            if p == self.last:
                p = self.new
            else:
                p += step

        return cy

    def calculPh(self, k, ph):

        val = self.phases49(k)

        jde     = val[0]
        E       = val[1]
        M       = val[2]
        Mp      = val[3]
        F       = val[4]
        omega49 = val[5]

        if   ph == self.new:
            jde += correctionNewMoon49(E, Mp, F, M, omega49)
        elif ph == self.first:
            jde += correctionFirstLastQuarter49(E, Mp, F, M, omega49)
            jde += correctionW49(E, M, Mp, F)
        elif ph == self.full:
            jde += correctionFullMoon49(E, Mp, F, M, omega49)
        elif ph == self.last:
            jde += correctionFirstLastQuarter49(E, Mp, F, M, omega49)
            jde -= correctionW49(E, M, Mp, F)
        else:
            print("Erreur Class PhaseMoon phases")

        jde -= self.DT    # TD to UT

        return jde + self.tz   # Time zone

    def phases49(self, k):

        # T is the time in Julian centuries since the epoch 2000. (49.3)
        T = k / 1236.85
        T2 = pow(T, 2)
        T3 = pow(T, 3)
        T4 = pow(T, 4)

        """ Calcul de JDE, E, M, Mp, F, omega49 respectivement. Chap. 49 """

        p = []

        # Times of the mean phases of the Moon
        p.append(self.JDE(k, T))

        # Eccentricity of the Earth's orbit around the Sun
        p.append(1 - 0.002516 * T - 0.0000074 * T2)

        # Sun's mean anomaly at time JDE
        p.append(2.5534 + (29.10535670 * k)\
                        - (0.0000014 * T2) \
                        - (0.00000011 * T3))

        # Moons mean anomaly
        p.append(201.5643 + (385.81693528 * k)\
                          + (0.0107582 * T2)  \
                          + (0.00001238 * T3) \
                          - (0.000000058 * T4))

        # Moon's argument of latitude
        p.append(160.7108 + (390.67050284 * k)\
                          - (0.0016118 * T2)  \
                          - (0.00000227 * T3) \
                          + (0.000000011 * T4))

        # Longitude of the ascending node of the lunar orbit
        p.append(124.7746 - (1.56375588 * k)\
                          + (0.0020672 * T2)\
                          + (0.00000215 * T3))

        return p

    # Times of the mean phases of the Moon
    def JDE(self, k, T):
        return 2451550.09766 + (29.530588861 * k)\
                             + (0.00015437 * pow(T, 2))\
                             - (0.000000150 * pow(T, 3))\
                             + (0.00000000073 * pow(T, 4))

    # Current phase
    def currentK(self, jd, k):

        do = True
        while do:
            k -= 0.25
            jde = self.JDE(k, k/1236.85)
            do = jde > jd
        return k

    # Update phases
    def upPhases(self, currentPhase):

        phases = [self.new, self.first, self.full, self.last]
        index = phases.index(currentPhase)
        step, i = 0.25, 0

        for n in range(len(phases)):
            phases[index] = i
            index += 1
            i += step
            if index == len(phases):
                index = 0

        return {"new": phases[0], "first": phases[1], "full": phases[2], "last": phases[3]}
