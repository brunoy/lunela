##
#         Passages of the Moon through the Nodes
#
#                       Chapter 51
##


# Moon modules
from .tables import termsNode51
from .functions import deltaT, jd2date
from .names import nameND

class Nodes:
    """ Calculation ascending and descending nodes. """
    def __init__(self, yd, tz):

        self.tz = tz

        self.year = yd
        self.k  = (self.year - 2000.05) * 13.4223   # (51.1)

        self.k = round(2 * self.k) / 2


    def nodes(self, nombre = 1):

        # Limit 200
        if nombre > 200: nombre = 200
        # Number = number of months
        nombre = (nombre + int(nombre / 7)) * 2

        k = self.k
        nodes = []

        for i in range(nombre):
            jde = self.arguments(k)
            if k - int(k):   # Ex  +124.5 ou -41.5 -> noeud descendant
                nodes.append(jd2date(jde) + (nameND[1],) )
            else:       # Ex +232.0 ou -12.0  -> noeud ascendant
                nodes.append(jd2date(jde) + (nameND[0],) )
            k += 0.5
        return nodes

    def f_K(self, year):
        return (year - 2000.05) * 13.4223

    def arguments(self, k):
        """ Calcul jde, D, M, Mp, omega, V, P, E  """

        # Time in Julian centuries since the epoch 2000.
        T  = k / 1342.23

        T2 = pow(T, 2)
        T3 = pow(T, 3)
        T4 = pow(T, 4)

        # Time
        jde = 2451565.1619 + (27.212220817 * k)\
                           + (0.0002762 * T2)\
                           + (0.000000021 * T3)\
                           - (0.000000000088 * T4)

        # Mean elongation of the moon
        D = 183.6380 + (331.73735682 * k)\
                     + (0.0014852 * T2)\
                     + (0.00000209 * T3)\
                     - (0.000000010 * T4)

        # Sun's mean anomaly
        M =  17.4006 + (26.82037250 * k)\
                     + (0.0001186 * T2)\
                     + (0.00000006 * T3)

        # Moon's mean anomaly
        Mp = 38.3776 + (355.52747313 * k)\
                     + (0.0123499 * T2)\
                     + (0.000014627 * T3)\
                     - (0.000000069 * T4)

        # Omega
        omega51 = 123.9767 - (1.44098956 * k)\
                           + (0.0020608 * T2)\
                           + (0.00000214 * T3)\
                           - (0.000000016 * T4)

        # V
        V = 299.75 + (132.85 * T) - (0.009173 * T2)

        # P
        P = omega51 + 272.75 - (2.3 * T)

        # Eccentricity of the Earth' orbit around the Sun (47.6)
        E = 1 - (0.002516 * T) - (0.0000074 * T2)

        # Calcul jde
        jde += termsNode51(D, M, Mp, V, P, E, omega51)

        # Convert jde  TD to Universal time
        jde -= (deltaT(self.year) / 86400)

        return jde + self.tz # Time zone
