##
#         File cfg, parameter, date time, timezone.
##


# Modules python
from os import mkdir, environ
from os.path import exists, isdir, join
import configparser
import sys
import datetime

# Modules lunela
from .functions import julianDay, jd2date, hms2jdec, decH2hm, anDecimal
from .rising import RisingSetting
from .release import NAME
from .names import image


def timedate(fmt):
    """ Init date, hour, time zone. """

    dic_time = {}

    now_pc = datetime.datetime.now().astimezone()
    now_utc = datetime.datetime.now(datetime.timezone.utc)

    dic_time['date_utc'] = now_utc.strftime('%d/%m/%Y')
    dic_time['hour_utc'] = now_utc.strftime('%H:%M:%S')

    if not fmt.upper() == 'UTC':
        tz = now_pc.strftime('%z')
        dic_time['date_pc'] = now_pc.strftime('%d/%m/%Y')
        dic_time['hour_pc'] = now_pc.strftime('%H:%M:%S')
    else:
        tz = now_utc.strftime('%z')
        dic_time['date_pc'] = now_utc.strftime('%d/%m/%Y')
        dic_time['hour_pc'] = now_utc.strftime('%H:%M:%S')


    # Format tz with ':' as separator
    dic_time['tz'] = tz[:3] + ':' + tz[-2:]

    # tz in day
    dic_time['deltaTZ'] = hms2jdec(dic_time['tz'] + ':00')

    return dic_time


def fileCfg():
    """ Parse file cfg. """

    userDatas = {}

    # Path file config
    filecfg  = NAME + ".cfg"

    path = join(
            environ.get('APPDATA') or
            environ.get('XDG_CONFIG_HOME') or
            join(environ['HOME'], '.config'),
            NAME + '/'
            )

    userDatas['cfgpath'] = path + filecfg

    # Init, parse fichier config
    configcfg(path, filecfg)
    cfg = readcfg(path + filecfg)

    # Init Latitude, longitude, format.
    userDatas['lat'] = round(float(cfg[0]), 6) if cfg[0] else 0
    userDatas['lon'] = round(float(cfg[1]), 6) if cfg[1] else 0
    userDatas['fmt'] = cfg[2][:7] if cfg[2] else "UTC"

    if not cfg[0] or not cfg[1]:
        userDatas['code_error'] = -2
    else: # invers long
        userDatas['lon'] *= -1

    # Between -180 to +180
    if userDatas['lon'] < -180 or userDatas['lon'] > 180:
        userDatas['code_error'] = -3

    # Between -90 to +90
    if userDatas['lat'] < -90 or userDatas['lat'] > 90:
        userDatas['code_error'] = -4

    return userDatas


def parameters(fmt):
    """ Handling parameters. """

    nb_args = len(sys.argv) - 1
    dic_arg = {}

    if nb_args == 0:
        dic_arg['arg'] = False
        return dic_arg

    # if -h or --help print usage.
    if sys.argv[1] == '-h' or sys.argv[1] == '--help':
        dic_arg['code_error'] = -9
        return dic_arg

    if nb_args == 1:
        dic_arg['hour_pc'] = '00:00:00'

    # Test time
    else:
        h = m = s = None
        lst = sys.argv[2].split(":")

        try:
            h = int(lst[0])

            if len(lst) > 1:
                m = int(lst[1])
            if len(lst) > 2:
                s = int(lst[2])
        except:
            dic_arg['code_error'] = -6
            return dic_arg

        # Test value hour
        if h > 23 or h < 0:
            dic_arg['code_error'] = -6
            return dic_arg
        elif h == 0:
            h = "00"

        if m:
            if m > 59 or m < 0:
                dic_arg['code_error'] = -6
                return dic_arg
        else:
            m = "00"

        if s:
            if s > 59 or s < 0:
                dic_arg['code_error'] = -6
                return dic_arg
        else:
            s = "00"
        # On 2 digits
        dic_arg['hour_pc'] = str(h).zfill(2) + ":" + str(m).zfill(2) + ":" + str(s).zfill(2)

    # Test value date
    arg1 = sys.argv[1].replace('-', '/')
    lst = arg1.split('/')

    try:
        d = int(lst[0])
        m = int(lst[1])
        y = int(lst[2])
    except:
        dic_arg['code_error'] = -5
        return dic_arg

    if d > 31 or d < 1:
        dic_arg['code_error'] = -5
        return dic_arg
    if m > 12 or m < 1:
        dic_arg['code_error'] = -5
        return dic_arg
    if y > 9999 or y < -2000:
        dic_arg['code_error'] = -7
        return dic_arg

    # On 2 digits
    dic_arg['date_pc'] = str(d).zfill(2) + "/" + str(m).zfill(2) + "/" + str(y)
    dic_arg['arg'] = True

    if fmt.upper() == 'UTC':

        dic_arg['date_utc'] = dic_arg['date_pc']
        dic_arg['hour_utc'] = dic_arg['hour_pc']
        dic_arg['tz'] = '+00:00'
        dic_arg['deltaTZ'] = 0
    else:
        # timezone
        dtz = datetime.datetime.now().astimezone()
        tz = dtz.strftime('%z')

        # Format tz
        dic_arg['tz'] = tz[:3] + ':' + tz[-2:]

        # tz in day
        dic_arg['deltaTZ'] = hms2jdec(dic_arg['tz'] + ':00')

        # Convert to pc hour
        jd = julianDay(dic_arg['date_pc']) + hms2jdec(dic_arg['hour_pc'])
        jd -= dic_arg['deltaTZ']
        dic_arg['date_utc'], dic_arg['hour_utc'] = jd2date(jd, SECONDS=True)
        dic_arg['date_utc'] = dic_arg['date_utc'].replace('-', '/')

    return dic_arg


def configcfg(path, filecfg):
    """ Make file config. """

    if not exists(path + filecfg):

        if not isdir(path):
            mkdir(path)
            from shutil import copy
            copy('image/distance.jpg', path + image.replace(" ", "_") + '.jpg')

        with open(path + filecfg, 'w') as configfile:
            configfile.write("[config]\n")
            configfile.write("latitude = \n")
            configfile.write("longitude = \n")
            configfile.write("# Time format, UTC (Universal time) or LOCAL (hour computer)\n")
            configfile.write("fmt = UTC")


def readcfg(path):
    """ Read the file config. """

    config = configparser.ConfigParser()
    config.read(path)
    cfg = []

    for parameter, value in config['config'].items():
        value = value.replace("\"", "")
        value = value.replace("\'", "")
        value = value.replace(",", ".")
        cfg.append(value)

    return cfg

def timezoneRisingSetting(data):
    """ Calcul hour moonris, moonset and transit in UTC or hour PC."""

    dic = {}

    # Update jd, year
    jd = julianDay(data['date_pc'])
    yearDecimal = anDecimal(data['date_pc'])

    # Init calcul rising, transit, setting today...
    moon = RisingSetting(jd, yearDecimal, data['deltaTZ'], data['lat'], data['lon'])

    # Init
    dic['rising']  = moon.rising()
    dic['setting'] = moon.setting()
    dic['risazim'] = None
    dic['setazim'] = None


    ####   UTC   ############################################"
    if data['deltaTZ'] == 0:

        # Init calcul day-1
        moon2 = RisingSetting(jd-1, yearDecimal, data['deltaTZ'], data['lat'], data['lon'])

        # Moon descending or ascending
        if moon.transitAlt() > moon2.transitAlt():
            dic['typ'] = 0
        else:
            dic['typ'] = 1

        # Moonris
        if dic['rising'] < 24:
            dic['rising'] = decH2hm(dic['rising'])
            dic['risazim'] = moon.risAzimuth()
        else: #No moonris in this day
            dic['rising'] = None

        # Moonset
        if dic['setting'] < 24:
            dic['setting'] = decH2hm(dic['setting'])
            dic['setazim'] = moon.setAzimuth()
        else: # No moonset in this day
            dic['setting'] = None

        return dic

    ####    hour local < utc   ##########################################
    if data['deltaTZ'] < 0:

        # Init calcul day+1
        moon2 = RisingSetting(jd+1, yearDecimal, data['deltaTZ'], data['lat'], data['lon'])

        # Moon descending or ascending
        if moon2.transitAlt() > moon.transitAlt():
            dic['typ'] = 0
        else:
            dic['typ'] = 1

        # Moonris
        if dic['rising'] < 0:
            dic['rising'] = moon2.rising()
            if dic['rising'] < 0:
                dic['rising'] = decH2hm(dic['rising']+24)
                dic['risazim'] = moon2.risAzimuth()
            else:
                dic['rising'] = None
        else:
            dic['rising'] = decH2hm(dic['rising'])
            dic['risazim'] = moon.risAzimuth()

        # Moonset
        if dic['setting'] < 0:
            dic['setting'] = moon2.setting()
            if dic['setting'] < 0:
                dic['setting'] = decH2hm(dic['setting']+24)
                dic['setazim'] = moon2.setAzimuth()
            else:
                dic['setting'] = None
        else:
            dic['setting'] = decH2hm(dic['setting'])
            dic['setazim'] = moon.setAzimuth()

        return dic


    ####    hour local > utc   ######################################
    if data['deltaTZ'] > 0:

        # Init calcul day-1
        moon2 = RisingSetting(jd-1, yearDecimal, data['deltaTZ'], data['lat'], data['lon'])

        # Moon descending or ascending
        if moon.transitAlt() > moon2.transitAlt():
            dic['typ'] = 0
        else:
            dic['typ'] = 1

        # Moonris
        if dic['rising'] > 24:
            dic['rising'] = moon2.rising()
            if dic['rising'] > 24:
                dic['rising'] = decH2hm(dic['rising']-24)
                dic['risazim'] = moon2.risAzimuth()
            else:
                dic['rising'] = None
        else:
            dic['rising'] = decH2hm(dic['rising'])
            dic['risazim'] = moon.risAzimuth()

        # Moonset
        if dic['setting'] > 24:
            dic['setting'] = moon2.setting()
            if dic['setting'] > 24:
                dic['setting'] = decH2hm(dic['setting']-24)
                dic['setazim'] = moon2.setAzimuth()
            else:
                dic['setting'] = None
        else:
            dic['setting'] = decH2hm(dic['setting'])
            dic['setazim'] = moon.setAzimuth()

        return dic

def JD_DD_DY(date, hour):
    """ Calcul Julian day, decimal day and decimal year. """

    dat = {}
    dat['jd'] = julianDay(date)
    dat['dayDecimal'] = hms2jdec(hour)
    dat['yearDecimal'] = anDecimal(date)
    return dat
