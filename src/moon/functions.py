##
#     Commons functions and utilities
##


# Python module
from math import sin, cos, tan, asin, atan2, radians, degrees, floor, modf


# Moon module
from .names import cardinaux, day



###   Commons functions   ####################################

def julianDay(date):            # Format jj/mm/années
                                # 1.5/01/-4712 ==> 0.0
    """ Convert date to Julian Day. Chapter 7 """

    lst=date.split('/')
    J = float(lst[0])
    M = int(lst[1])
    A = int(lst[2])
    b = 0

    if M <= 2:
        A -= 1
        M += 12

    """ Check if date is in gregorian calendar or not (julian calendar).
        the day following 1582 October 4 (Julian calendar) is 1582 October
        15 (Gregorian calendar). """

    gregorian = True
    if A < 1582:
        gregorian = False
    elif A == 1582:
        if M < 10:
            gregorian = False
        elif M == 10:
            if J <= 4:
                gregorian = False
            elif J < 15:
                # No date between 5 and 14 October 1582.
                # No error issue, consider in Gregorian calendar
                pass

    if  gregorian:
        a = int(A/100)
        b = 2 - a + int(a/4)

    return int(365.25 * (A + 4716)) + int(30.6001 * (M + 1)) + J + b - 1524.5


def jd2date(JD, SECONDS=False):
    """ Convert Julian day to date and hour. Chapter 7. """
    JD += 0.5
    Z = int(JD)
    F = JD - Z
    if Z < 2299161:
        A = Z
    else:
        alpha = int((Z - 1867216.25) / 36524.25)
        A = Z + 1 + alpha - int(alpha / 4)
    B = A + 1524
    C = int((B - 122.1) / 365.25)
    D = int(365.25 * C)
    E = int((B - D) /30.6001)

    Jdec = B - D - int(30.6001 * E) + F         # calcul du jour décimal

    J = int(Jdec)                               # calcul du jour

    if (E < 14):                                # calcul du mois
        M = E - 1
    else:
        M = E - 13

    if (M > 2):                                 # calcul de l'année
        A = C - 4716
    else:
        A = C - 4715

    # Ajout Heure , minute, secondes
    hdec = Jdec - J
    h = int(hdec * 24)                          # Calcul de l'heure

    mdec = (hdec * 24) - h
    m = int(mdec * 60)                          # Calcul des Minutes

    sdec = (mdec * 60) - m
    s = int(sdec * 60)                         # Calcul des secondes

    return formatt([J,M,A], [h,m,s], SECONDS)


def height(ha, decli, lat):
    """ Altitude in degrees. Arguments: hour angle, declination and latitude """
    h  = sin(radians(lat)) * sin(radians(decli))
    h += cos(radians(lat)) * cos(radians(decli)) * cos(radians(ha))
    return degrees(asin(h))


def azimut(ha, decli, lat):
    """ Calcul azymuth (13.5) """

    a = sin(radians(ha))
    b = cos(radians(ha)) * sin(radians(lat))
    b -= tan(radians(decli)) * cos(radians(lat))
    ab = degrees(atan2(a,b))

    if ab < 0: ab += 360
    return ab


def deltaT(y):
    """
           POLYNOMIAL EXPRESSIONS FOR DELTA T (ΔT)
           For convert dynamical time in universal time. ΔT = TD - UT
           Source : eclipse.gsfc.nasa.gov/SEcat5/deltatpoly.html
    """

    year = int(y)

    if year < -500:     # Before 500
        u = (y - 1820) / 100
        return -20 + 32 * u * u

    elif year < 500:    # Between -500 +500
        u = y / 100
        return 10583.6 - 1014.41 * u + 33.78311 * pow(u, 2) - 5.952053 * pow(u, 3)\
             - 0.1798452 * pow(u, 4) + 0.022174192 * pow(u, 5) + 0.0090316521 * pow(u, 6)

    elif year < 1600:   # Between +500 and +1600
        u = (y-1000)/100
        return 1574.2 - 556.01 * u + 71.23472 * pow(u, 2) + 0.319781 * pow(u, 3)\
             - 0.8503463 * pow(u, 4) - 0.005050998 * pow(u, 5) + 0.0083572073 * pow(u, 6)

    elif year < 1700:   # Between +1600 +1700
        t = y - 1600
        return 120 - 0.9808 * t - 0.01532 * pow(t, 2) + pow(t, 3) / 7129

    elif year < 1800:   # Between +1700 +1800
        t = y - 1700
        return 8.83 + 0.1603 * t - 0.0059285 * pow(t, 2)\
             + 0.00013336 * pow(t, 3) - pow(t, 4) / 1174000

    elif year < 1860:   # Between +1800 +1860
        t = y - 1800
        return 13.72 - 0.332447 * t + 0.0068612 * pow(t, 2) + 0.0041116 * pow(t, 3)\
             - 0.00037436 * pow(t, 4) + 0.0000121272 * pow(t, 5) - 0.0000001699 * pow(t, 6)\
             + 0.000000000875 * pow(t, 7)

    elif year < 1900:   # Between +1860 +1900
        t = y - 1860
        return 7.62 + 0.5737 * t - 0.251754 * pow(t, 2) + 0.01680668 * pow(t, 3) \
             - 0.0004473624 * pow(t, 4) + pow(t, 5) / 233174

    elif year < 1920:   # Between +1900 +1920
        t = y - 1900
        return -2.79 + 1.494119 * t - 0.0598939 * pow(t, 2)\
              + 0.0061966 * pow(t, 3)- 0.000197 * pow(t, 4)

    elif year < 1941:   # Between +1920 +1941
        t = y - 1920
        return 21.20 + 0.84493 * t - 0.076100 * pow(t, 2) + 0.0020936 * pow(t, 3)

    elif year < 1961:   # Between +1941 +1961
        t = y - 1950
        return 29.07 + 0.407 * t - pow(t, 2) / 233 + pow(t, 3) / 2547

    elif year < 1986:   # Between +1961 +1986
        t = y - 1975
        return 45.45 + 1.067 * t - pow(t, 2) / 260 - pow(t, 3) / 718

    elif year < 2005:   # Between +1986 +2005
        t = y - 2000
        return 63.86 + 0.3345 * t - 0.060374 * pow(t, 2) + 0.0017275 * pow(t, 3)\
             + 0.000651814 * pow(t, 4) + 0.00002373599 * pow(t, 5)

    elif year < 2050:   # Between +2005 +2050
        t = y - 2000
        return 62.92 + 0.32217 * t + 0.005589 * pow(t, 2)

    elif year < 2150:   # Between +2050 +2150
        u = (y - 1820) / 100
        return -20 + 32 * pow(u, 2) - 0.5628 * (2150 - y)

    else:               # After +2150
        u = (y - 1820) / 100
        return -20 + 32 * u * u

#####   Utilities   ###################################################################

def reduce360(x):
    """ Reduce angle to multiple between 0 and 360. """
    return ( x - 360 * floor(x / 360) )


def anDecimal(date):
    """ Convert date (eg, '10/6/2020') to decimal year. Chap. 7 """
    lst = date.split('/')

    J = float(lst[0])
    M = int(lst[1])
    A = int(lst[2])

    if bissextile(A):
        k = 1
        j = 366
    else:
        k = 2
        j = 365

    numJ = int((275 * M)/9) - k * int((M + 9) / 12) + J -30
    return A + (numJ / j )


def bissextile(an):
    """ Determine if leap year. """
    if not (an % 400) or (not (an % 4) and (an % 100) ):
        return True
    else:
        return False


def decD2dms(dd):
    """ Convert decimal degrees to degrees minutes seconds """
    sign = ''

    if dd < 0:
        sign = '-'
        dd *= -1

    d = int(dd)
    m = int((dd - d) * 60)
    s = (((dd - d) * 60) - m) * 60
    s = int(s)

    return sign + str(d) + '° ' + str(m) + '\' ' + str(s) + '\"'


def decH2hms(hd):
    """ Convert decimal hours to hours minutes seconds """
    h = int(hd)
    m = int((hd - h) * 60)
    s = (((hd - h) * 60) - m) * 60
    s = int(s)
    return str(h) + 'h ' + str(m) + 'm ' + str(s) + 's'


def hms2jdec(hour):
    """ Convert hours (Ex, '12:01:16') to decimal day. """
    lst = hour.split(':')
    H = float(lst[0])
    M = float(lst[1])
    S = float(lst[2])

    S = S / 60
    M = (M + S) / 60
    return (H + M) / 24


def decJ2jhm(jour):
    """ Convert decimal days to days, hours, minutes (moon age) """
    v, j = modf(jour)
    v, h = modf(v * 24)
    v, m = modf(v * 60)

    return str(int(j)) + day[0] + ' ' + str(int(h)) + 'h ' + str(int(m)) + 'm'


def decH2hm(hd):    # Moonris ,moonset...
    """ Convert decimal hour to hours, minutes. """
    h = int(hd)
    m = int((hd - h) * 60)
    s = (((hd - h) * 60) - m) * 60

    if s > 30:
        m += 1
        if m == 60:
            m = 0
            h += 1
            if h == 24: h = 0

    return str(h).zfill(2) + 'h' + str(m).zfill(2) + 'm'


def pt_cardinaux(az):
    """ Calculation and determination of the cardinal point. """

    az += (360 / len(cardinaux)) / 2
    az = (az / 360) * len(cardinaux)

    if az > len(cardinaux): az = 0

    return cardinaux[int(az)]


def formatt(date, hour, SECONDS):
    """ Date and time format with 2 digits. """

    sep = '/' if date[2] < 0 else '-'

    date = str(date[0]).zfill(2) + sep + str(date[1]).zfill(2) + sep + str(date[2])

    if SECONDS:
        hour = str(hour[0]).zfill(2) + ':' + str(hour[1]).zfill(2) + ':' + str(hour[2]).zfill(2)
    else:
        hour = str(hour[0]).zfill(2) + ':' + str(hour[1]).zfill(2)

    return date, hour
