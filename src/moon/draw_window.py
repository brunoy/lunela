# -*- coding:Utf-8 -*-

# Modules python
import curses

# Modules moon's
from . import release
from . import draw_init
from .position import Ephemeride
from .phases import Phases
from .apogee import PerigeeApogee
from .node import Nodes
from .equinox import EquinoxSolstice
from .solar import DistanceEarthSun
from .names import nameTY, risset, cardinaux

# Module translate
import gettext
t = gettext.translation(release.NAME, 'locale', fallback=True)
_ = t.gettext


def drawCursesWindow(stdscr):

    # Initialisation curses
    data = init_curses(stdscr)
    if data['code_error']:
        return data['code_error']

    # Size term
    height, width = stdscr.getmaxyx()
    halfH, halfW = height // 2, width // 2

    # Parse file cfg
    data.update( draw_init.fileCfg() )
    if data['code_error']:

        if not data['code_error'] == -2:
            return data['code_error']

        data['code_error'] = 0
        # if not latitude longitude, print message
        message(stdscr, width, halfH, data['cfgpath'])
        stdscr.refresh()

    # Parse parameters if there are
    data.update( draw_init.parameters(data['fmt']) )
    if data['code_error']:
        return data['code_error']

    if not data['arg']:
        # Init date, time
        data.update( draw_init.timedate(data['fmt']) )

    # Calcul Julian day, decimal day, year decimal
    data.update( draw_init.JD_DD_DY(data['date_utc'], data['hour_utc']) )

    # Calcul moon's data
    ephemeride = Ephemeride(data['jd'] + data['dayDecimal'], data['lat'], data['lon'])
    phase = Phases(data['jd'], data['dayDecimal'], data['yearDecimal'], data['deltaTZ'])
    val = EquinoxSolstice(data['yearDecimal'], data['deltaTZ'])
    solar = DistanceEarthSun(data['jd'] + data['dayDecimal'])

    # Create pad
    padH = 29
    pad = curses.newpad(padH, width)

    # Draw on stdscr...
    # Name prog
    nameProg(stdscr, width)
    # Bar foot
    barFoot(stdscr, width, height, data['arg'])

    # Draw on pad ...
    # Rising, transit, setting, current phase
    part1(pad, width, halfW, data, phase, data['lat'], data['lon'])

    # Datas
    part2(pad, 6, width, halfW, ephemeride, phase.ageMoon())

    # Phases, nodes, apogee, perigee
    part3(pad, width, halfW, halfH, data['yearDecimal'], data['deltaTZ'], phase.cycle())

    # Distance earth sun, size sun.
    part4(pad, 20, width, halfH, halfW, solar.distanceSunAU(), solar.appDSun())

    # Equinoxes solstices
    part5(pad, width, halfH, halfW, val.equinoxSol())

    # Define new windows
    # Bar title
    win1 = curses.newwin(2, width, 1, 0)
    barTitle(win1, width, data['date_pc'], data['hour_pc'], data['tz'])

    # For update moonrise, moonset
    winpart1 = curses.newwin(5, width, 0, 0)

    # For update datas
    winpart2 = curses.newwin(6, width, 0, 0)

    # for update distance earth sun, size sun.
    winpart4 = curses.newwin(3, width, 0, 0)


    # Print
    padY = 3
    h = height - 3
    win1.refresh()
    pad.refresh(0, 0, padY, 0, h, width)
    # User interaction
    day_now = day(data['date_pc'])
    k = y  = 0
    maxY = padH - h
    while k != ord('q') and k != ord('Q'):

        if k == curses.KEY_DOWN and y <= maxY:
            y += 1
            pad.refresh(y, 0, padY, 0, h, width)
        elif k == curses.KEY_UP and y > 0:
            y -= 1
            pad.refresh(y, 0, padY, 0, h, width)

        elif k == ord('r') or k == ord('R'):
            if data['arg'] == False:

                # Clear
                win1.clear()

                # Réinitialisation
                data.update( draw_init.timedate(data['fmt']) )
                data.update( draw_init.JD_DD_DY(data['date_utc'], data['hour_utc']) )

                ephemeride = Ephemeride(data['jd'] + data['dayDecimal'], data['lat'], data['lon'])
                solar = DistanceEarthSun(data['jd'] + data['dayDecimal'])
                phase = Phases(data['jd'], data['dayDecimal'], data['yearDecimal'], data['deltaTZ'])

                day_up = day(data['date_pc'])
                if day_up != day_now:
                    day_now = day_up
                    # Update hour moonris, moonset
                    part1(winpart1, width, halfW, data, phase, data['lat'], data['lon'])
                    winpart1.overwrite(pad, 0, 0, 0, 0, 4, width-1)

                part2(winpart2, 0, width, halfW, ephemeride, phase.ageMoon())
                part4(winpart4, 0, width, halfH, halfW, solar.distanceSunAU(), solar.appDSun())

                # Update bar title
                barTitle(win1, width, data['date_pc'], data['hour_pc'], data['tz'])

                # Update pad
                winpart2.overwrite(pad, 0, 0, 6, 0, 11, width-1)
                winpart4.overwrite(pad, 0, 0, 20, 0, 22, width-1)

                # print
                win1.refresh()
                pad.refresh(y, 0, padY, 0, h, width)

        k = stdscr.getch()

    close_curses()

    return 0

# Curses functions draw windows

def nameProg(stdscr, width):
    name = release.NAME
    stdscr.attron(curses.A_BOLD)
    stdscr.addstr(0, 0, ' ' * width, curses.color_pair(7) )
    stdscr.addstr(0, center(width, name), name, curses.color_pair(7) )
    stdscr.attroff(curses.A_BOLD)


def barTitle(window, width, date, hour, tz):
    """ Curses draw bar title. """

    strTitle = _("Ephemeris of the moon")
    title = "{} {} {} {}".format(strTitle, date.replace('/', '-'), hour, tz)
    xc = center(width, title)

    window.attron(curses.color_pair(7))
    window.addstr(0,0, " " * (width))
    window.addstr(0, xc, title)
    window.attroff(curses.color_pair(7))


def barFoot(stdscr, width, height, arg):
    """ Curses draw bar foot. """

    r = "[R] > " + _("Refresh")
    d = "[▼] > " + _("Down")
    u = "[▲] > " + _("Up")
    q = "[Q] > " + _("Quit")

    w = width - len(d) - len(u) - len(q) - 2 # sep

    if arg == False:
        w -= len(r) - 1
        w = w // 8
        w *= " "
        form = "{}{}{}|{}{}{}|{}{}{}|{}{}{}".format(w, r, w, w, d, w, w, u, w, w, q, w)
    else:
        w = w // 6
        w *= " "
        form = "{}{}{}|{}{}{}|{}{}{}".format(w, d, w, w, u, w, w, q, w)

    stdscr.attron(curses.color_pair(7))
    stdscr.addstr(height - 1, 0, ' ' * (width-1))
    stdscr.addstr(height - 1, 0, form)
    stdscr.attroff(curses.color_pair(7))


def part1(pad, width, halfW, data, phase, lat, lon):
    """ Curses Moonris, moonset, phase. """

    y = 0

    # Latitude, longitude
    pad.attron(curses.color_pair(5))
    lt = _("Latitude") + ' = ' + str(lat)
    lg = _("Longitude") + ' = ' + str(lon*-1)
    w = width // 3
    pad.addstr(y, w - (len(lt)//2), lt)
    pad.addstr(y, width - (len(lg)//2) - w, lg)

    pad.attroff(curses.color_pair(5))

    # Phases moon ascii u+1F311 à u+1F318
    asciiphase = ['🌑','🌒', '🌓', '🌔', '🌕', '🌖', '🌗', '🌘', '🌑']

    # Phase name
    y = 2
    xc = center(halfW, phase.name())
    pad.addstr(y, xc, phase.name(), curses.color_pair(6))

    # moonris , moonset, azimuth
    moon = draw_init.timezoneRisingSetting(data)

    if moon['rising']:
        lever = "{} {}".format(moon['rising'], moon['risazim'])
    else:   # No moonrise
        lever = "{}".format(risset[0])

    if moon['setting']:
        coucher = "{} {}".format(moon['setting'], moon['setazim'])
    else:   # No moonset
        coucher = "{}".format(risset[1])

    # Check if day begin by moonris or moonset
    if moon['rising'] == None:
        moonrisBegin = False
    elif moon['setting'] == None:
        moonrisBegin = True
    elif moon['rising'] < moon['setting']:
        moonrisBegin = True
    else:
        moonrisBegin = False

    # Descendante or montante
    moontype = nameTY[moon['typ']]

    # name
    nameR = _("rising")
    nameS = _("Setting")
    longNameR = len(nameR)
    longNameS = len(nameS)

    # Center x
    xl  = center(halfW, "{} : {}".format(nameR, lever))
    xc  = center(halfW, "{} : {}".format(nameS, coucher))

    if moonrisBegin:
        xc += halfW
    else:
        xl += halfW

    # moon phase ascii
    pad.addstr(y, halfW, asciiphase[phase.num()])

    # Title
    pad.addstr(y+2, xc, "{} :".format(nameS))
    pad.addstr(y+2, xl, "{} :".format(nameR))

    # Values
    pad.attron(curses.color_pair(6))
    pad.addstr(y, center(halfW, moontype) + halfW, moontype)
    pad.addstr(y+2, xl + longNameR + 3, "{}".format(lever))
    pad.addstr(y+2, xc + longNameS + 3, "{}".format(coucher))
    pad.attroff(curses.color_pair(6))


def part2(pad, y, width, halfW, ephem, age):
    """ Curse moon's data. """

    # Lines
    pad.hline(y, 0, curses.ACS_HLINE, width)
    pad.vline(y+2, halfW, curses.ACS_VLINE, 4)

    # Title
    nameD = _("Datas")
    pad.addstr(y, center(width, nameD), " {} ".format(nameD))

    # Keys cols left
    keysL = ( _('Altitude'), _('Azimuth'), _('Right ascension'), _('Declination') )

    maxi = 0
    y += 2

    for i in keysL:
        if maxi < len(i): maxi = len(i)

    for i in range(len(keysL)):
        pad.addstr(y+i, 3, keysL[i])
        pad.addstr(y+i, maxi+4, ":")

    # Color
    pad.attron(curses.color_pair(2))

    # Print values left
    maxi += 6
    altitude = ephem.altitude()

    # length
    maxc = 0
    for i in cardinaux:
        if maxc < len(i): maxc = len(i)
    if maxc < len(_("Not visible")):
        maxc = len(_("Not visible"))

    # Del
    pad.addstr(y+1, maxi, maxc * " ")

    # if negative altitude, moon not visible
    if altitude[0] == '-':
        pad.addstr(y+1, maxi, _("Not visible"))
    else:
        pad.addstr(y+1, maxi, ephem.azimuth())

    # Delete
    pad.addstr(y, maxi, 12 * " ")
    pad.addstr(y+2, maxi, 11 * " ")
    pad.addstr(y+3, maxi, 12 * " ")
    # Print
    pad.addstr(y, maxi, altitude)
    pad.addstr(y+2, maxi, ephem.rightAscension())
    pad.addstr(y+3, maxi, ephem.declination())

    # Color off
    pad.attroff(curses.color_pair(2))


    # Cols right
    keysR = ( _('Illumination'), _('Age'), _('Distance earth'), _('Apparent size') )

    maxi = 0

    for i in keysR:
        if maxi < len(i): maxi = len(i)

    for i in range(len(keysR)):
        pad.addstr(y+i, halfW + 4, keysR[i])
        pad.addstr(y+i, halfW + maxi + 5, ":")

    # Color on
    pad.attron(curses.color_pair(2))

    maxi += 7
    # Delete
    pad.addstr(y  , halfW+maxi, 6 * " ")
    pad.addstr(y+1, halfW+maxi, 11 * " ")

    # Print values right
    pad.addstr(y  , halfW+maxi, "{:.1f} %".format(ephem.illumin()))
    pad.addstr(y+1, halfW+maxi, age)
    pad.addstr(y+2, halfW+maxi, "{:.0f} km".format(ephem.distance()))
    pad.addstr(y+3, halfW+maxi, "{:.2f}°".format(ephem.appDMoon()))

    # Color off
    pad.attroff(curses.color_pair(2))


def part3(pad, width, halfW, halfH, yearDecimal, dtz, phase):
    """ Curses window phases, perigee, apogee and nodes. """

    node = Nodes(yearDecimal, dtz).nodes()
    apog = PerigeeApogee(yearDecimal, dtz).apogeePerigee()

    y = 13

    # Line
    pad.hline(y, 0, curses.ACS_HLINE, width)
    pad.vline(y+2, halfW, curses.ACS_VLINE, 4)

    # Title
    title = _("Phases")
    pad.addstr(y, center(halfW, title), " {} ".format(title))
    title = _("Apogee perigee nodes")
    pad.addstr(y, center(halfW, title) + halfW, " {} ".format(title))

    long_chaine = len("dd-mm-yyyy 00:00")

    # Phase
    for i in range(len(phase)):
        pad.addstr(y+2+i, 3, "{} {}".format(phase[i][0], phase[i][1]))

    # Color on
    pad.attron(curses.color_pair(4))

    for i in range(len(phase)):
        pad.addstr(y+2+i, 3 + long_chaine, " {}".format(phase[i][2]))

    # Color off
    pad.attroff(curses.color_pair(4))

    # Apogee, perigee nodes
    for i in range(len(apog)):
        pad.addstr(y+2+i, halfW+4, "{} {}".format(apog[i][0], apog[i][1]))
        pad.addstr(y+4+i, halfW+4, "{} {}".format(node[i][0], node[i][1]))

    for i in range(len(apog)):
        pad.addstr(y+2+i, long_chaine+halfW+4, " {}".format(apog[i][2]), curses.color_pair(5))
        pad.addstr(y+4+i, long_chaine+halfW+4, " {}".format(node[i][2]), curses.color_pair(6))


def part4(pad, y, width, halfH, halfW, sunEarth, diamSun):
    """ curses, distance earth to sun and apparent size of the sun """

    # Lines
    pad.hline(y, 0, curses.ACS_HLINE, width)

    # String
    str1 = _("Distance Earth to Sun")
    str2 = _("Apparent size sun")

    title = _("Others")
    pad.addstr(y, center(width, title), " {} ".format(title))
    pad.addstr(y+2, 3, "{} :".format(str1))
    pad.addstr(y+2, halfW + 3, "{} :".format(str2))

    # Color on
    pad.attron(curses.color_pair(3))
    # AU: Astronomical unit
    unit = _("AU")
    pad.addstr(y+2, 5 + len(str1), " {:.7f} {}".format(sunEarth, unit))
    pad.addstr(y+2, halfW+5+len(str2), " {:.2f}°".format(diamSun))

    # Color off
    pad.attroff(curses.color_pair(3))


def part5(pad, width, halfH, halfW, equinox):
    """ Curses Equinoxes solstices """

    y = 24
    # Line
    pad.hline(y, 0, curses.ACS_HLINE, width)

    title = _("Equinoxes solstices")
    pad.addstr(y, center(width, title), " {} ".format(title))

    x1 = len("00-00-0000 00:00 ")

    pad.addstr(y+2, 3, "{} {}".format(equinox[0][0], equinox[0][1]))
    pad.addstr(y+3, 3, "{} {}".format(equinox[1][0], equinox[1][1]))
    pad.addstr(y+2, halfW, "{} {}".format(equinox[2][0], equinox[2][1]))
    pad.addstr(y+3, halfW, "{} {}".format(equinox[3][0], equinox[3][1]))

    # Color on
    pad.attron(curses.color_pair(1))
    pad.addstr(y+2, 3+x1, "{}".format(equinox[0][2]))
    pad.addstr(y+2, halfW+x1, "{}".format(equinox[2][2]))
    pad.addstr(y+3, 3+x1, "{}".format(equinox[1][2]))
    pad.addstr(y+3, halfW+x1, "{}".format(equinox[3][2]))
    # Color off
    pad.attroff(curses.color_pair(1))


def message(stdscr, width, halfH, cfgpath):
    """ Invite to fill in the cfg file """

    message0 = _("Please,")
    message1 = _("enter your geographic coordinates,")
    message2 = _("longitude and latitude, in decimal degrees")
    message3 = _("in ") + cfgpath
    message4 = _("Press any key")

    stdscr.addstr(halfH-4, center(width, message0), message0)
    stdscr.addstr(halfH-2, center(width, message1), message1)
    stdscr.addstr(halfH  , center(width, message2), message2)
    stdscr.addstr(halfH+2, center(width, message3), message3)
    stdscr.addstr(halfH+4, center(width, message4), message4)
    stdscr.getch()
    stdscr.clear()

def usage():
    name = release.NAME + ' ' + str(release.VERSION + release.REVISION)

    print("\n{}\t\t{}".format(name, _("Print ephemeris of the moon in a")))
    print("\t\t\t{}\n".format(_("terminal.")))
    print("\t\t\t{}".format(_("The calculations, formules come from the book")))
    print("\t\t\t{}\n".format(_("«Astronomical algorithms» by Jean Meeus.")))
    print("[{}]\t\t{}".format(_("Options"), '-' * 45))
    print("\n-h, --help\t\t{}\n".format(_("Print this usage.")))
    print("dd/mm/yyyy\t\t{}".format(_("First parameter may be a date in")))
    print("\t\t\t{}\n".format(_("this format.")))
    print("hh:mm:ss\t\t{}".format(_("And, if date in parameter, the second")))
    print("\t\t\t{}".format(_("parameter may be hour in this format")))
    print("\t\t\t{}\n".format(_("( or hh:mm or hh ).")))

def init_curses(stdscr):
    """ Init datas, curses. """

    curs = {}
    curs['code_error'] = 0

    # Initialisation curses
    curses.initscr()
    curses.noecho()
    curses.cbreak()
    curses.curs_set(False)

    # Colors
    curses.start_color()
    curses.init_pair(1, curses.COLOR_RED, curses.COLOR_BLACK)
    curses.init_pair(2, curses.COLOR_GREEN, curses.COLOR_BLACK)
    curses.init_pair(3, curses.COLOR_YELLOW, curses.COLOR_BLACK)
    curses.init_pair(4, curses.COLOR_BLUE, curses.COLOR_BLACK)
    curses.init_pair(5, curses.COLOR_MAGENTA, curses.COLOR_BLACK)
    curses.init_pair(6, curses.COLOR_CYAN, curses.COLOR_BLACK)
    curses.init_pair(7, curses.COLOR_BLACK, curses.COLOR_WHITE)
    curses.init_pair(8, curses.COLOR_WHITE, curses.COLOR_BLUE)

    # Window size
    size = stdscr.getmaxyx()

    # Minimum size required 80*8
    if size[1] < 76 or size[0] < 10:
        curs['code_error'] = -1

    return curs

def close_curses():
    curses.echo()
    curses.nocbreak()
    curses.curs_set(True)
    curses.endwin()

def center(width, string):
    """ Center in width. """
    return int((width // 2) - (len(string) // 2) - len(string) % 2)

def day(date):
    lst = date.split('/')
    return lst[0]
