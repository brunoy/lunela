##
#           Perigee and apogee of the Moon.
#
#                     Chapter 50
##


# Moon modules
from .tables import correctionApogee50A, correctionPerigee50A
from .functions import jd2date, deltaT
from .names import nameAP


class PerigeeApogee:
    """ Calculation perigee and apogee """
    def __init__(self, yd, tz):

        self.tz = tz

        self.k = (yd - 1999.97) * 13.2555 # (50.2)
        self.k  = round(self.k * 2) / 2

        self.year = int(yd)

        # To convert TD in UT
        self.deltaT = deltaT(yd) / 86400



    def apogeePerigee(self, nombre = 1):

        # Limit 200
        if nombre > 200: nombre = 200
        nombre = nombre + int(nombre / 8)

        ap = []
        k = self.k

        for i in range(nombre*2):

            if k - int(k):      # Perigee
                jde = self.calculAP(k, True)
                ap.append(jd2date(jde) + (nameAP[1],) )
            else:               # Apogee
                jde = self.calculAP(k, False)
                ap.append(jd2date(jde) + (nameAP[0],) )

            k += 0.5

            # Recalcul deltaT if year =!
            if int((k / 13.2555) + 1999.97) > self.year:
                self.year += 1
                self.deltaT = deltaT(self.year) / 86400

        return ap

    def calculAP(self, k, apogee):
        args = self.arguments(k)
        jde = args[0]
        D = args[1]
        M = args[2]
        F = args[3]
        T = args[4]
        if apogee:
            jde += correctionApogee50A(D, M, F, T)
        else:
            jde += correctionPerigee50A(D, M, F, T)

        # TD to universal time
        jde -= self.deltaT
        return jde + self.tz

    def arguments(self, k):
        """ Calcul jde, D, M, F respectivement. """

        T = k / 1325.55 # (50.3)

        args = []

        # Time in Julian centuries since the epoch 2000.0 (50.1)
        args.append(2451534.6698 + (27.55454989 * k)\
                                 - (0.0006691 * pow(T, 2))\
                                 - (0.000001098 * pow(T, 3))\
                                 + (0.0000000052 * pow(T, 4)))

        # Moon's mean elongation at time JDE
        args.append(171.9179 + (335.9106046 * k)\
                             - (0.0100383 * pow(T, 2))\
                             - (0.00001156 * pow(T, 3))\
                             + (0.000000055 * pow(T, 4)))

        # Sun's mean anomaly
        args.append(347.3477 + (27.1577721 * k)\
                             - (0.0008130 * pow(T, 2))\
                             - (0.0000010 * pow(T, 3)))

        # Moon's argument of latitude
        args.append(316.6109 + (364.5287911 * k)\
                             - (0.0125053 * pow(T, 2))\
                             - (0.0000148 * pow(T, 3)))

        args.append(T)

        return args

