##
#    Moon:  rising, transit and setting
#
#                   Chapter 15
##


# Python module
from math import sin, cos, asin, acos, radians, degrees

# Lunela modules
from .functions import height, azimut, deltaT, reduce360, decH2hm, pt_cardinaux
from .position import Ephemeride
from .names import cardinaux

class RisingSetting:
    """ Moon rising, transit and setting. """
    def __init__(self, jd, yd, dtz, lat, lon):

        self.lat, self.lon = lat, lon

        # Delta time zone
        self.deltaTZ = dtz

        self.T = (jd - 2451545) / 36525
        self.year = int(yd)

        # Right ascension and declination day -1, day and day +1
        day = Ephemeride(jd - 1, self.lat, self.lon)
        self.alpha1, self.decli1 = day.ra_decli()

        day  = Ephemeride(jd, self.lat, self.lon)
        self.alpha2, self.decli2 = day.ra_decli()

        # Equatorial horizontal parallax of the moon
        self.p = degrees(asin(6378.14 / day.distance()))

        # Sidereal time at greenwich
        self.theta = day.theta()

        day = Ephemeride(jd + 1,  self.lat, self.lon)
        self.alpha3, self.decli3 = day.ra_decli()

        # Ajust
        if self.alpha3 < self.alpha2: self.alpha2 -= 360
        if self.alpha2 < self.alpha1: self.alpha1 -= 360

        self.m0, self.m1, self.m2 = self.calcul()

    def calcul(self):
        """ Calcul moonris moonset transit. """

        ho = (0.7275 * self.p) - 0.56666            # Variation Moon
        H0 = self.fH0(ho, self.decli2, self.lat)    # 15.1
        DT = deltaT(self.year)                      # DT = TD - UT

        # Calcul de m : m1 -> rising, m0 -> transit, m2 -> setting
        m0, m1, m2 = self.m(self.alpha2, self.theta, H0, self.lon)

        # Calcul sidereal time, in degrees
        thm0 = self.theta + 360.985647 * m0
        thm1 = self.theta + 360.985647 * m1
        thm2 = self.theta + 360.985647 * m2

        thm0 = reduce360(thm0)
        thm1 = reduce360(thm1)
        thm2 = reduce360(thm2)

        # Calcul for interpolate alpha
        a = self.alpha2 - self.alpha1
        b = self.alpha3 - self.alpha2
        c = b - a
        alIn1 = self.interpolated(self.alpha2, a, b, c, m1 + DT/86400)   # Rising
        alIn2 = self.interpolated(self.alpha2, a, b, c, m0 + DT/86400)   # Transit
        alIn3 = self.interpolated(self.alpha2, a, b, c, m2 + DT/86400)   # Setting

        # Calcul for interpolate decli
        a = self.decli2 - self.decli1
        b = self.decli3 - self.decli2
        c = b - a
        deIn1 = self.interpolated(self.decli2, a, b, c, m1 + DT/86400)   # Rising
        deIn3 = self.interpolated(self.decli2, a, b, c, m2 + DT/86400)   # Setting

        # Local four angle
        H1 = thm1 - self.lon - alIn1            # Rising
        H2 = thm0 - self.lon - alIn2            # Transit
        H3 = thm2 - self.lon - alIn3            # Setting

        # altitude
        h1 = height(H1, deIn1, self.lat)        # Rising
        h3 = height(H3, deIn3, self.lat)        # Setting

        # Corrections to m
        Dm0 = - (H2/360)                              # Transit
        Dm1 = self.dm(h1, ho, deIn1, H1, self.lat)    # Rising
        Dm2 = self.dm(h3, ho, deIn3, H3, self.lat)    # Setting

        # m corrected
        m0 += Dm0
        m1 += Dm1
        m2 += Dm2

        return(m0, m1, m2)


    def rising(self):
        """ Hour moonrise """
        ris = (self.m1 + self.deltaTZ) * 24
        return ris


    def setting(self):
        """ Hour moonset """
        sett = (self.m2 + self.deltaTZ) * 24
        return sett


    def transitAlt(self):
        """ Transit altitude """
        thetaTransit = self.siderealTime(self.m0)
        return height(thetaTransit - self.lon - self.alpha2, self.decli2, self.lat)


    def risAzimuth(self):
        """ Rising azimuth """

        thetaRising  = self.siderealTime(self.m1)

        # Local hour angle (Chap. 13)
        h = thetaRising - self.lon - self.alpha2
        return pt_cardinaux(azimut(h, self.decli2, self.lat))


    def setAzimuth(self):
        """ Setting azimuth """

        thetaSetting = self.siderealTime(self.m2)

        # Local hour angle (same as above)
        h = thetaSetting - self.lon - self.alpha2
        return pt_cardinaux(azimut(h, self.decli2, self.lat))


    def interpolated(self, arg, a, b, c, n):
        return arg + ((n/2) * (a + b + (n * c)))

    def siderealTime(self, m):
        """ Sidereal time, chap. 12 """

        arg = 1.00273790935

        # Hour
        h  = (m * 86400 * arg) / 3600

        # Degrees
        h *= 15

        return reduce360(h + self.theta)


    # Chapitre 15 Rising, transit and setting #
    # Delta m
    def dm(self, h, ho, dec, H, lat):
        a = h - ho
        b = 360 * cos(radians(dec)) * cos(radians(lat)) * sin(radians(H))
        return a / b


    def fH0(self, ho, decli2, lat):
        """ Formule 15.1 find Ho for calculate m """
        a = sin(radians(ho)) - sin(radians(lat)) * sin(radians(decli2))
        b = cos(radians(lat)) * cos(radians(decli2))
        return degrees(acos(a/b))


    def m(self, alpha2, theta, H0, lon):
        """ Formule 15.2 calcul three values m. """

        m0 = (alpha2 + lon - theta) / 360   # Transit
        if m0 < 0:
            m0 = 1 + m0
        elif m0 > 1:
            m0 = m0 - 1

        m1 = m0 - (H0 / 360)                # Rising
        if m1 < 0:
            m1 = 1 + m1
        elif m1 > 1:
            m1 = m1 - 1

        m2 = m0 + (H0 / 360)                # Setting
        if m2 < 0:
            m2 = 1 + m2
        elif m2 > 1:
            m2 = m2 - 1

        return (m0, m1, m2)
