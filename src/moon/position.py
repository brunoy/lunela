##
#       Position of the Moon Chapter 47
##

# Python module
from math import sin, cos, tan, asin, atan2, radians, degrees

# Lunela modules
from .functions import height, azimut, reduce360,  decD2dms,  decH2hms, pt_cardinaux
from .tables import sumr47A, suml47A, sumb47B


class Ephemeride:
    def __init__(self, jd, lat, lon):

        self.lat = lat

        T = (jd - 2451545) / 36525  #(22.1)
        T2 = pow(T, 2)
        T3 = pow(T, 3)
        T4 = pow(T, 4)

        tab = self.arguments47(T, T2, T3, T4)

        Lp = tab[0]             # Moon's mean longitude
        self.D = tab[1]         # Mean elongation of the Moon
        self.M = tab[2]         # Sun's mean anomaly
        self.Mp = tab[3]        # Moon's mean anomaly
        F  = tab[4]             # Moon's argument of latitude
        E  = tab[5]             # Eccentricity of the Earth's orbit around the Sun.

        # Table 47.A : somme r
        sumr = sumr47A(self.D, self.M, self.Mp, F, E)
        # Table 47.A : somme l
        suml = suml47A(self.D, self.M, self.Mp, F, E)
        # Table 47.B : somme b
        sumb = sumb47B(self.D, self.M, self.Mp, F, E)

        self.lamb = Lp + (suml/1000000)         # Apparent longitude of the Moon
        self.beta = sumb/1000000                # Apparent latitude of the Moon
        self.delt = 385000.56 + (sumr  / 1000)  # Distance center Earth / Moon in kilometers

        # Equatorial horizontal parallax of the moon
        self.p = degrees(asin(6378.14 / self.delt))

        # True obliquity ecliptic Earth and nutation in longitude
        self.epsi, deltaPsi = self.arguments22(T, T2, T3)

        # Apparent longitude of the Moon
        self.lamb += deltaPsi

        # Sidereal time
        self.theTa = self.thet(T2, T3, jd)

        # Moon's apparent right ascension and declination
        self.alpha = self.ra(self.lamb, self.epsi, self.beta)
        self.decli = self.dec(self.lamb, self.epsi, self.beta)

        self.Ha = self.theTa - lon - self.alpha          # local hour angle
        self.h  = height(self.Ha, self.decli, lat)       # altitude -> DiamApparent

    ##  Values    ##################################################
    #Right ascension and declination decimales for calculs
    def ra_decli(self):
        return (self.alpha, self.decli)

    # Right ascension
    def rightAscension(self):
        if self.alpha < 0:
            return decH2hms( (self.alpha + 360) / 15 )
        return decH2hms(self.alpha / 15)

    # Déclination
    def declination(self):
        return decD2dms(self.decli)

    # Distance center Earth / Moon in kilometers
    def distance(self):
        return self.delt

    # altitude
    def altitude(self):
        return decD2dms(self.h)

    # azimuth
    def azimuth(self):
         return pt_cardinaux(azimut(self.Ha, self.decli, self.lat))


    # illumination
    def illumin(self):
        return self.illum(self.D, self.M, self.Mp) * 100

    # Sidereal time
    def theta(self):
        return self.theTa

    # Apparent diameter Moon
    def appDMoon(self): # Chap. 55
        d = (358473400 / self.delt) / 3600
        m = 1 + sin(radians(self.h)) *  sin(radians(self.p))
        return d * m * 2

    ## Calculations ...   #######################################################
    def arguments47(self, T, T2, T3, T4):  # Chap. 47
        """ Calcul Lp, D, M, Mp, F, E respectivement. """
        f = []

        # Moon's mean longitude (47.1)
        f.append(reduce360(218.3164477 + (481267.88123421 * T)\
                                       - (0.0015786 * T2)\
                                       + (T3 / 538841)   \
                                       - (T4 / 65194000)))

        # Mean elongation of the moon (47.2)
        f.append(reduce360(297.8501921 + (445267.1114034 * T)\
                                       - (0.0018819 * T2)\
                                       + (T3 / 545868)   \
                                       - (T4 / 113065000)))

        # Sun's mean anomaly (47.3)
        f.append(reduce360(357.5291092 + (35999.0502909 * T)\
                                       - (0.0001536 * T2)\
                                       + (T3 / 24490000)))

        # Moon's mean anomaly (47.4)
        f.append(reduce360(134.9633964 + (477198.8675055 * T)\
                                       + (0.0087414 * T2)\
                                       + (T3 / 69699)    \
                                       - (T4 / 14712000)))

        # Moon's argument of latitude (mean distance of the Moon from its ascending node) (47.5)
        f.append(reduce360(93.2720950  + (483202.0175233 * T)\
                                       - (0.0036539 * T2)\
                                       - (T3 / 3526000)  \
                                       + (T4 / 863310000)))

        # Eccentricity of the Earth's orbit around the Sun (47.6)
        f.append( 1 - (0.002516 * T) - (0.0000074 * T2) )

        return f

    def arguments22(self, T, T2, T3):  # Chap. 22
        """ Calcul Epsi delta psi """

        # Longitude of the ascending node of the Moon's mean orbit on the ecliptic.
        omega22 = 125.04452 - (1934.136261 * T)

        # Mean longitude of the Sun
        L22 = 280.4665 + (36000.7698 * T)

        # Mean longitude of the Moon
        Lp22 = 218.3165 + (481267.8813 * T)

        # Mean obliquity ecliptic
        Eo = 23.43929111111111 - (0.0130041666 * T)\
                               - (0.00000016388 * T2)\
                               - (0.00000050361 * T3)

        # Nutation in longitude en arc second deltaPsi
        DP  = -17.20 * sin(radians(omega22))
        DP -=  1.32  * sin(radians(L22 + L22))          # -1.32 livre . modifié
        DP -=  0.23  * sin(radians(Lp22 + Lp22))
        DP +=  0.21  * sin(radians(omega22 + omega22))
        DP /= 3600  # arc seconds to degrees

        # Nutation in obliquity en arc second delta epsi
        DE  = 9.20 * cos(radians(omega22))
        DE += 0.57 * cos(radians(L22 + L22))
        DE += 0.10 * cos(radians(Lp22 + Lp22))
        DE -= 0.09 * cos(radians(omega22 + omega22))
        DE /= 3600  # Arc seconds to degrees

        # return Epsi(Eo + delta epsi) and delta psi)
        return (Eo + DE, DP)

    # Sidereal time at greenwich (12.4)
    def thet(self, T2, T3, jd):
        return reduce360(280.46061837 + (360.98564736629 * (jd - 2451545.0))\
                      + (0.000387933 * T2 - (T3 / 38710000)))

    # Moon's apparent right ascension (13.3)
    def ra(self, lamb, epsi, beta):
        a  = sin(radians(lamb)) * cos(radians(epsi))
        a -= tan(radians(beta)) * sin(radians(epsi))
        b  = cos(radians(lamb))
        return degrees(atan2(a, b))

    # Declination of the Moon (13.4)
    def dec(self, lamb, epsi, beta):
        d  = sin(radians(beta)) * cos(radians(epsi))
        d += cos(radians(beta)) * sin(radians(epsi)) * sin(radians(lamb))
        return degrees(asin(d))

    # Illuminated Fraction of the Moon's disk. Chap. 48
    def illum(self, D, M, Mp):
        i  =  180 - D
        i += -6.289 * sin(radians(Mp))
        i +=  2.100 * sin(radians(M))
        i += -1.274 * sin(radians(D + D - Mp))
        i += -0.658 * sin(radians(D + D))
        i += -0.214 * sin(radians(Mp + Mp))
        i += -0.110 * sin(radians(D))
        return (1 + cos(radians(i))) / 2
