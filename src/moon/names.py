from .release import NAME

# Translate
import gettext
t = gettext.translation(NAME, 'locale', fallback=True)
_ = t.gettext


# Name image
image = _("outstretched arm")

# For moonris, moonset
risset = (_("No moonrise"), _("No moonset"), _("No transit"))


# For age moon
day = _("days")


# Type moon
nameTY = (_("Ascending moon"), _("Descending moon"))

# For equinoxe solstice
nameES = (_("March equinox"), _("June solstice"), _("September equinox"), _("December solstice"))


# For node
nameND = (_("Ascending node"), _("Descending node"))


# For périgée apogée ...
nameAP = (_("Perigee"), _("Apogee"))


# For phase quart
phase = {0.00: _("New moon"),
         0.25: _("First quarter"),
         0.50: _("Full moon"),
         0.75: _("Last quarter")}

# Phases
namePhase = ( _("New moon"), _("Waxing crescent"), _("First quarter"),
              _("Waxing gibbous"), _("Full moon"), _("Waning gibbous"),
              _("Third quarter"),  _("Waning crescent"), _("New moon"))


# Compass points from south (chap. 13)
cardinaux = (_("south"), _("south-southwest"), _("southwest"), _("west-southwest"),
              _("west"),  _("west-northwest"),  _("northwest"), _("north-northwest"),
              _("north"), _("north-northeast"), _("northeast"), _("east-northeast"),
              _("east"),  _("east-southeast"),  _("southeast"), _("south-southeast"))


code_error = (_("Minimum terminal dimensions required"),
              _("File cfg longitude"),
              _("File cfg latitude"),
              _("Error writing date in argument"),
              _("Error writing hour in argument"),
              _("The year must be between -2000 and +9999"))

