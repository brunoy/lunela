##
#       Solar coordinate
#
#          Chapter 25
##

# Python module
from math import sin, cos, radians


class DistanceEarthSun:
    """ Distance earth to sun. """
    def __init__(self, jd):

        # Time in Julian centuries (25.1)
        T  = (jd - 2451545.0) / 36525
        T2 = pow(T, 2)

        # Geometric mean longitude of the sun (25.2)
        #Lo = 280.46646 + 36000.76983 * T + 0.0003032 * T2      # Not need

        # Mean anomaly of the sun (25.3)
        M  = 357.52911 + 35999.05029 * T - 0.0001537 * T2

        # Eccentriciy of the Earth's orbit (25.4)
        e  = 0.016708634 - 0.000042037 * T - 0.0000001267 * T2

        # Sun's equation of the center C
        C  = self.c(T, T2, M)

        #stl = Lo + C    # Sun's true longitude                 # Not need
        sta = M + C     # Sun's true anomaly

        self.R = self.f_R(sta, e)

    def distanceSunKM(self):
        # Constant: one astronomical unit in km
        AU = 149597870
        # Distance in km
        return self.R * AU

    def distanceSunAU(self):
        # Distance in AU
        return self.R

    # Apparent diameter sun, chap. 55
    def appDSun(self):
        return ( (959.63 / 3600) / self.R ) * 2

    # Sun's equation of the center C
    def c(self, T, T2, M):
        c  = (1.914602 - 0.004817 * T - 0.000014 * T2) * sin(radians(M))
        c += (0.019993 - 0.000101 * T) * sin(radians(M + M))
        c += 0.000289 * sin(radians(M + M + M))
        return c

    # Distance between the centers of the sun and the Earth, in astronomical units (25.5)
    def f_R(self, v, e):
        return (1.000001018 * (1 - pow(e, 2))) / (1 + e * cos(radians(v)))

