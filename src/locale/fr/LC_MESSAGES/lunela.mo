��    Q      �  m   ,      �     �     �  %   �          %     :     H     Z     i     x     �     �     �     �     �     �     �     �     �               7     V     h      {     �  	   �     �     �     �  $   �     	     	  
   	  
   "	     -	     9	     @	     H	     P	      ^	     	     �	     �	     �	     �	     �	  -   �	  (   �	     
     -
     0
     @
     O
     _
     n
     s
     x
     �
  "   �
     �
  *   �
     �
     �
     �
  	     	        "  $   3     X     _     e     u  	   �  	   �     �     �     �     �  *   �  >  �     3     G  (   J     s     {     �     �     �     �     �     �     �                    0     F     U     Y     s     �     �  "   �  #   �  .        3     D     P     a     r  $   �     �     �     �     �     �     �  	   �     �  %   �  -        M     _     g     s     �     �  *   �  1   �                     (     D     V     o     u     y     �  &   �     �  +   �     �     �     �       
     
   "  ,   -     Z     `     d     p     ~  	   �  
   �     �     �     �  +   �     4      ?          *                  8   O   #         :   Q         P      2   ;   B   N       +            E   !                    <      %   J               @      9   '   5   I          K   3              F   /   A      0                  "             G       H       D   1   (           6   )       -      .                        
              C   M   L   >      ,   	   &   =       7       $    ( or hh:mm or hh ). AU And, if date in parameter, the second Apogee Apogee perigee nodes Apparent size Apparent size sun Ascending moon Ascending node Azimuth Datas December solstice Declination Descending moon Descending node Distance Earth to Sun Distance earth Down Ephemeris of the moon Equinoxes solstices Error writing date in argument Error writing hour in argument File cfg latitude File cfg longitude First parameter may be a date in First quarter Full moon June solstice Last quarter March equinox Minimum terminal dimensions required New moon No moonrise No moonset No transit Not visible Others Perigee Please, Press any key Print ephemeris of the moon in a Print this usage. Quit Refresh Right ascension September equinox Setting The calculations, formules come from the book The year must be between -2000 and +9999 Third quarter Up Waning crescent Waning gibbous Waxing crescent Waxing gibbous days east east-northeast east-southeast enter your geographic coordinates, in  longitude and latitude, in decimal degrees north north-northeast north-northwest northeast northwest outstretched arm parameter may be hour in this format rising south south-southeast south-southwest southeast southwest this format. west west-northwest west-southwest «Astronomical algorithms» by Jean Meeus. Project-Id-Version: lunela 1.0
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-03-02 20:59+0100
Last-Translator:  <bruno@mailoo.org>
Language-Team: French <traduc@traduc.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 ( ou hh:mm ou hh ). UA Et, si une date en paramètre, le second Apogée Apogée périgée noeuds Taille apparente Taille apparente soleil Lune montante Noeud ascendant Azimut Données Solstice de décembre Déclinaison Lune descendante Noeud descendant Distance terre soleil Distance terre Bas Éphémérides de la lune Équinoxes solstices Erreur date en paramètre Erreur heure en paramètre Erreur latitude fichier lunela.cfg Erreur longitude fichier lunela.cfg Le premier paramètre peut-être une date dans Premier quartier Pleine lune Solstice de juin Dernier quartier Équinoxe de mars Dimensions terminal minimum requises Nouvelle lune Pas de lever Pas de coucher Non Non visible Autres Périgée Svp, Appuyez sur une touche pour continuer Affiche les éphémérides de la lune dans un Affiche cet usage Quitter Rafraîchir Ascension droite Équinoxe de septembre Coucher Les calculs, formules proviennent du livre L'année doit-être comprise entre -2000 et +9999 Dernier quartier Haut Dernier croissant Lune gibbeuse décroissante Premier croissant Lune gibbeuse croissante jours est est-nord-est est-sud-est entrez vos coordonnées géographiques dans  longitude et latitude, en degrés décimals nord nord-nord-est nord-nord-ouest nord-est nord-ouest bras tendu paramètre peut-être l'heure dans ce format Lever sud sud-sud-est sud-sud-ouest sud-est sud-ouest ce format. ouest ouest-nord-ouest ouest-sud-ouest «Astronomical algorithms» par Jean Meeus. 