# lunela

* Displays the ephemeris of the moon in a terminal (with curses).
* It works only for linux.

### Sources
* Except for the calculation of the delta to convert dynamic time into universal time,
  which comes from the NASA website ( eclipse.gsfc.nasa.gov/SEcat5/deltatpoly.html ),
  all other calculations are from the book «Astronomical algorithms» by Jean Meeus.

### lunela.cfg
* The first start program create a file.cfg in /home/user/.config/lunela .
* Please, write your longitude and latitude in it, in decimals degrees for the moonris, moonset, altitude and azimuth.
* Choose UTC (by default) or LOCAL ( hour PC ) .

### Usage
lunela -h, --help

### Langage
* English
* French

## Manuel build

### Dépendances programme   :
* python >=3.6

For install, download sources, open a terminal in directory of sources:
```
# install.sh
```

For uninstall :
```
# uninstall.sh
```

## Screenshot
![2021-03-23_11_38_16_498x324_scrot.png](https://gitlab.com/brunoy/lunela/wikis/uploads/7dc976c1e477bca7f77e3100440ae36e/2021-03-23_11_38_16_498x324_scrot.png)
