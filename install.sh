#!/bin/sh

#
# Install lunela
# Bruno Rome <bruno (at) mailoo (dot) org>
# https://gitlab.com/brunoy/lunela.git
#

name='lunela'
filepath='/usr/share/'
execpath='/usr/bin/'

# Prepare
rm -rf $name
mkdir $name
cp -r src/* $name
# Permissions
chown -R root: $name
chmod -R 644 $name
find $name -type d -exec chmod 755 {} \;
# Copy
cp -r --preserve=mode,ownership $name $filepath


# Executable
chown root: bin/$name
chmod 755 bin/$name
cp --preserve=mode,ownership bin/$name $execpath


# Cleanup
rm -r $name

exit 0
