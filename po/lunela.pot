# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-03-23 00:34+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/moon/draw_window.py:169
msgid "Ephemeris of the moon"
msgstr ""

#: src/moon/draw_window.py:182
msgid "Refresh"
msgstr ""

#: src/moon/draw_window.py:183
msgid "Down"
msgstr ""

#: src/moon/draw_window.py:184
msgid "Up"
msgstr ""

#: src/moon/draw_window.py:185
msgid "Quit"
msgstr ""

#: src/moon/draw_window.py:204
msgid "Latitude"
msgstr ""

#: src/moon/draw_window.py:205
msgid "Longitude"
msgstr ""

#: src/moon/draw_window.py:247
msgid "rising"
msgstr ""

#: src/moon/draw_window.py:248
msgid "Setting"
msgstr ""

#: src/moon/draw_window.py:284
msgid "Datas"
msgstr ""

#: src/moon/draw_window.py:288
msgid "Altitude"
msgstr ""

#: src/moon/draw_window.py:288
msgid "Azimuth"
msgstr ""

#: src/moon/draw_window.py:288
msgid "Right ascension"
msgstr ""

#: src/moon/draw_window.py:288
msgid "Declination"
msgstr ""

#: src/moon/draw_window.py:311 src/moon/draw_window.py:312
#: src/moon/draw_window.py:319
msgid "Not visible"
msgstr ""

#: src/moon/draw_window.py:337
msgid "Illumination"
msgstr ""

#: src/moon/draw_window.py:337
msgid "Age"
msgstr ""

#: src/moon/draw_window.py:337
msgid "Distance earth"
msgstr ""

#: src/moon/draw_window.py:337
msgid "Apparent size"
msgstr ""

#: src/moon/draw_window.py:379
msgid "Phases"
msgstr ""

#: src/moon/draw_window.py:381
msgid "Apogee perigee nodes"
msgstr ""

#: src/moon/draw_window.py:416
msgid "Distance Earth to Sun"
msgstr ""

#: src/moon/draw_window.py:417
msgid "Apparent size sun"
msgstr ""

#: src/moon/draw_window.py:419
msgid "Others"
msgstr ""

#. AU: Astronomical unit
#: src/moon/draw_window.py:427
msgid "AU"
msgstr ""

#: src/moon/draw_window.py:442
msgid "Equinoxes solstices"
msgstr ""

#: src/moon/draw_window.py:465
msgid "Please,"
msgstr ""

#: src/moon/draw_window.py:466
msgid "enter your geographic coordinates,"
msgstr ""

#: src/moon/draw_window.py:467
msgid "longitude and latitude, in decimal degrees"
msgstr ""

#: src/moon/draw_window.py:468
msgid "in "
msgstr ""

#: src/moon/draw_window.py:469
msgid "Press any key"
msgstr ""

#: src/moon/draw_window.py:481
msgid "Print ephemeris of the moon in a"
msgstr ""

#: src/moon/draw_window.py:482
msgid "terminal."
msgstr ""

#: src/moon/draw_window.py:483
msgid "The calculations, formules come from the book"
msgstr ""

#: src/moon/draw_window.py:484
msgid "«Astronomical algorithms» by Jean Meeus."
msgstr ""

#: src/moon/draw_window.py:485
msgid "Options"
msgstr ""

#: src/moon/draw_window.py:486
msgid "Print this usage."
msgstr ""

#: src/moon/draw_window.py:487
msgid "First parameter may be a date in"
msgstr ""

#: src/moon/draw_window.py:488
msgid "this format."
msgstr ""

#: src/moon/draw_window.py:489
msgid "And, if date in parameter, the second"
msgstr ""

#: src/moon/draw_window.py:490
msgid "parameter may be hour in this format"
msgstr ""

#: src/moon/draw_window.py:491
msgid "( or hh:mm or hh )."
msgstr ""

#: src/moon/names.py:10
msgid "outstretched arm"
msgstr ""

#: src/moon/names.py:13
msgid "No moonrise"
msgstr ""

#: src/moon/names.py:13
msgid "No moonset"
msgstr ""

#: src/moon/names.py:13
msgid "No transit"
msgstr ""

#: src/moon/names.py:17
msgid "days"
msgstr ""

#: src/moon/names.py:21
msgid "Ascending moon"
msgstr ""

#: src/moon/names.py:21
msgid "Descending moon"
msgstr ""

#: src/moon/names.py:24
msgid "March equinox"
msgstr ""

#: src/moon/names.py:24
msgid "June solstice"
msgstr ""

#: src/moon/names.py:24
msgid "September equinox"
msgstr ""

#: src/moon/names.py:24
msgid "December solstice"
msgstr ""

#: src/moon/names.py:28
msgid "Ascending node"
msgstr ""

#: src/moon/names.py:28
msgid "Descending node"
msgstr ""

#: src/moon/names.py:32
msgid "Perigee"
msgstr ""

#: src/moon/names.py:32
msgid "Apogee"
msgstr ""

#: src/moon/names.py:36 src/moon/names.py:42 src/moon/names.py:44
msgid "New moon"
msgstr ""

#: src/moon/names.py:37 src/moon/names.py:42
msgid "First quarter"
msgstr ""

#: src/moon/names.py:38 src/moon/names.py:43
msgid "Full moon"
msgstr ""

#: src/moon/names.py:39
msgid "Last quarter"
msgstr ""

#: src/moon/names.py:42
msgid "Waxing crescent"
msgstr ""

#: src/moon/names.py:43
msgid "Waxing gibbous"
msgstr ""

#: src/moon/names.py:43
msgid "Waning gibbous"
msgstr ""

#: src/moon/names.py:44
msgid "Third quarter"
msgstr ""

#: src/moon/names.py:44
msgid "Waning crescent"
msgstr ""

#: src/moon/names.py:48
msgid "south"
msgstr ""

#: src/moon/names.py:48
msgid "south-southwest"
msgstr ""

#: src/moon/names.py:48
msgid "southwest"
msgstr ""

#: src/moon/names.py:48
msgid "west-southwest"
msgstr ""

#: src/moon/names.py:49
msgid "west"
msgstr ""

#: src/moon/names.py:49
msgid "west-northwest"
msgstr ""

#: src/moon/names.py:49
msgid "northwest"
msgstr ""

#: src/moon/names.py:49
msgid "north-northwest"
msgstr ""

#: src/moon/names.py:50
msgid "north"
msgstr ""

#: src/moon/names.py:50
msgid "north-northeast"
msgstr ""

#: src/moon/names.py:50
msgid "northeast"
msgstr ""

#: src/moon/names.py:50
msgid "east-northeast"
msgstr ""

#: src/moon/names.py:51
msgid "east"
msgstr ""

#: src/moon/names.py:51
msgid "east-southeast"
msgstr ""

#: src/moon/names.py:51
msgid "southeast"
msgstr ""

#: src/moon/names.py:51
msgid "south-southeast"
msgstr ""

#: src/moon/names.py:54
msgid "Minimum terminal dimensions required"
msgstr ""

#: src/moon/names.py:55
msgid "File cfg longitude"
msgstr ""

#: src/moon/names.py:56
msgid "File cfg latitude"
msgstr ""

#: src/moon/names.py:57
msgid "Error writing date in argument"
msgstr ""

#: src/moon/names.py:58
msgid "Error writing hour in argument"
msgstr ""

#: src/moon/names.py:59
msgid "The year must be between -2000 and +9999"
msgstr ""
